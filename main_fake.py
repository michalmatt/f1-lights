import time
from yeelight import discover_bulbs, Bulb
from datetime import timedelta
from src.f1_lights.light_bulb import *
from yeelight.transitions import *
from yeelight import Flow


# === Alternative main for presentation purposes only ===
"""Whole main_fake.py can be deleted once not needed
Purpose is to omit calling external REST API and work on fake data (assumption of 1h remaining to F1 race start time)
Differences to main.py:
- will always work (regardless if F1 race is today or not)
- remaining time starts at 1h and decreases by 4min every 3sec"""


fake_remaining_time = timedelta(hours=1)

while fake_remaining_time > timedelta(minutes=0):
    if fake_remaining_time <= timedelta(hours=1):
        bulbs_list = discover_bulbs()
        bulb_id = 0
        bulb = Bulb(discover_bulbs()[bulb_id]['ip'])
        bulb.turn_on()
        bulb.set_brightness(100)
        bulb.set_rgb(0, 255, 0)
        while fake_remaining_time > timedelta(minutes=0):
            if timedelta(minutes=20) < fake_remaining_time <= timedelta(minutes=40):
                bulb.set_rgb(0, 0, 255)
            elif timedelta(minutes=10) < fake_remaining_time <= timedelta(minutes=20):
                bulb.set_rgb(255, 0, 0)
            elif timedelta(minutes=0) < fake_remaining_time <= timedelta(minutes=10):
                flow_strobe_color = Flow(count=0, transitions=strobe_color(brightness=100))
                bulb.start_flow(flow_strobe_color)
            time.sleep(3)
            fake_remaining_time -= timedelta(minutes=4)
        bulb.stop_flow()
        set_my_rgb(bulb)
        time.sleep(3)
        bulb.turn_off()
    time.sleep(1)
