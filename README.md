# f1-lights

Python (PTN) project on Polish-Japanese Academy of Information Technology

Based on YeeLight library: https://yeelight.readthedocs.io/en/latest/

MVP audience assumptions:
1. [must-have] Have at least 1 smart light bulb with WiFi module connected to the network
2. [must-have] Have a platform to invoke python file from on a daily basis (e.g. RaspberryPi, home server/VM)
3. [nice-to-have] Formula 1 races fans who does not want to miss any race
