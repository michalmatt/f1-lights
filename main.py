import time
from yeelight import discover_bulbs, Bulb
from datetime import timedelta
from src.f1_lights.f1_races_info import *
from src.f1_lights.light_bulb import *
from yeelight.transitions import *
from yeelight import Flow


# === Main program code ===
"""Should be added in OS (e.g. crone in Linux based system) to start every morning
bulb_id - index of the light bulb we want to operate on (by default 0)
Gets in the loop only if F1 race is scheduled for a given day and remaining time to the race is more than 0min
If that is true (F1 race is scheduled for today and it is still before the race) then:
a) Every 1h it will be checked whether it is already 1h or less to the F1 race
b) It above is true (and it is 1h or less to the F1 race) then program:
- turns the light bulb ON and sets its color to green
- sets bulb color to blue if time to F1 race is between 5 and 30min
- sets bulb color to red if time to F1 race is between 0 and 5min
Such a check is done every  1min.
After F1 race is started light bulb color is set to normal and then turned OFF."""


while get_race_remaining_time_today() is not None and get_race_remaining_time_today() > timedelta(minutes=0):
    if get_race_remaining_time_today() <= timedelta(hours=1):
        bulbs_list = discover_bulbs()
        bulb_id = 0
        bulb = Bulb(bulbs_list[bulb_id]['ip'])
        bulb.turn_on()
        bulb.set_brightness(100)
        bulb.set_rgb(0, 255, 0)
        while get_race_remaining_time_today() > timedelta(minutes=0):
            if timedelta(minutes=5) < get_race_remaining_time_today() <= timedelta(minutes=30):
                bulb.set_rgb(0, 0, 255)
            elif timedelta(minutes=1) < get_race_remaining_time_today() <= timedelta(minutes=5):
                bulb.set_rgb(255, 0, 0)
            elif timedelta(minutes=0) < get_race_remaining_time_today() <= timedelta(minutes=1):
                flow_strobe_color = Flow(count=0, transitions=strobe_color(brightness=100))
                bulb.start_flow(flow_strobe_color)
            time.sleep(60)
        bulb.stop_flow()
        set_my_rgb(bulb)
        bulb.turn_off()
    time.sleep(3600)
