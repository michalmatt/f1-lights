import unittest
from src.f1_lights.current_date_time import get_current_date_utc


class TestCurrentDateTime(unittest.TestCase):

    def test_get_current_date_utc(self):
        date = '2020-07-03'
        self.assertFalse(date == get_current_date_utc())


if __name__ == '__main__':
    unittest.main()
