import unittest
from src.f1_lights.f1_races_info import get_races_list


class TestF1RacesInfo(unittest.TestCase):

    def test_get_races_list(self):
        self.assertTrue(isinstance(get_races_list(), list))


if __name__ == '__main__':
    unittest.main()
