# === API and functions for F1 races info ===
"""
get_race_list() - always gets current F1 races data from external API
get_race_remaining_time_today() - if race is planned for today returns remaining time, if not returns: None
"""


import requests
from src.f1_lights.current_date_time import *


def get_races_list():
    url = "http://ergast.com/api/f1/current.json"
    r = requests.get(url)
    data = r.json()
    races_list = data['MRData']['RaceTable']['Races']
    return races_list


def get_race_remaining_time_today():
    races_list = get_races_list()
    current_date = get_current_date_utc()
    current_time = get_current_time_utc()
    remaining_time = None
    fmt = '%H:%M:%S'
    for x in races_list:
        if x['date'] == current_date:
            race_time = x['time'].replace('Z', '')
            remaining_time = datetime.strptime(race_time, fmt) - datetime.strptime(current_time, fmt)
    return remaining_time
