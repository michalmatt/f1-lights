# === UTC date and time functions ===
"""
get_current_date_utc() - returns current utc date in yyyy-mm-dd format
get_current_time_utc() - returns current utc time in hh-mm-ss format
"""


from datetime import datetime


def get_current_date_utc():
    return datetime.utcnow().strftime("%Y-%m-%d")


def get_current_time_utc():
    return datetime.utcnow().strftime("%H:%M:%S")
