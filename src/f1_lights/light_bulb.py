# === Light bulbs functions ===
"""
save_bulb_current_properties(bulb, my_dict) - clears given dictionary and saves current bulb properties
is_power_on(bulb) - check if given bulb is ON
set_my_rgb(bulb) - hard-coded rgb values which I consider "normal light"
(after MVP phase it should be changed to take initial rgb value)
"""


def save_bulb_current_properties(bulb, my_dict):
    my_dict.clear()
    my_dict.update(bulb.get_properties())
    return my_dict


def is_power_on(bulb):
    power_on = False
    if bulb.get_properties()['power'] == 'on':
        power_on = True
    return power_on


def get_rgb_value(bulb):
    return bulb.get_properties()['rgb']


def get_bright_value(bulb):
    return bulb.get_properties()['bright']


def set_my_rgb(bulb):
    red = 100
    green = 69
    blue = 39
    return bulb.set_rgb(red, green, blue)
